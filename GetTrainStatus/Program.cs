﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
namespace GetTrainStatus
{
    class Program
    {
        bool trainFound;
        static void Main(string[] args)
        {
            bool theStatus;
            string trainNo;
            string result;
            Program P = new Program();
            Console.Write("Enter a train no : ");
            trainNo = Console.ReadLine();
            theStatus = P.IsUpdatedTimeBehind(trainNo);
            if (theStatus) result = "Yes"; else result = "No";
            if (P.trainFound)
                Console.WriteLine("Is the updated time behind the current time ? " + result);
            else
                Console.WriteLine("There doesn't exists a train with a given train no.");
            Console.ReadLine();//To make sure the command prompt doesnt close immediately
        }

        private bool IsUpdatedTimeBehind(string trainNo)
        {
            //Creation of HttpWebRequest and HttpWebResponse instance

            string uri = "http://119.81.208.134/api/trains/livestatusall?trainno="+trainNo+"&doj=07-09-2018&getNames=true";
            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader responseReader = new StreamReader(response.GetResponseStream());
            //Deserializing the resultant object using the JsonConvert Class
            dynamic resultmsg = JsonConvert.DeserializeObject(responseReader.ReadToEnd());
            //Closing the stream and response instance
            response.Close();
            responseReader.Close();
            //Check The trainDataFound field in the response to see if train exists or not
            if (resultmsg.trainDataFound == "trainRunningDataFound") trainFound = true; else trainFound = false;

            if (trainFound)
            {

                //Getting the lastUpdate field from the response object
                string liveStatus = resultmsg.lastUpdated;
                //Creating the date string
                DateTime date = DateTime.Now;
                //Changing the format of the current date time to the response updated time format
                //string dateString = date.Day + " " + date.ToString("MMM") + " " + date.Year + " " + date.Hour + ":" + date.Minute;
                string dateString = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                //Converting the liveStatus to the DateTime format
                DateTime updatedTime = Convert.ToDateTime(liveStatus);
                //Calculating difference in minutes between the two Dates.
                TimeSpan span = date.Subtract(updatedTime);
                int differenceInMinutes = span.Days * 1440 + span.Hours * 60 + span.Minutes;
                //Getting the threshold value from the App.config file
                int threshold = Convert.ToInt32(ConfigurationManager.AppSettings["Threshold"]);

                // Results

                Console.WriteLine("TrainNo : " + trainNo);
                Console.WriteLine("TrainName : " + resultmsg.trainName);
                Console.WriteLine("The Current Time : " + dateString);
                Console.WriteLine("Last Updated Time : " + updatedTime);
                Console.WriteLine("Threshold Value : " + threshold);
                Console.WriteLine("The Current Time - Last Updated Time = " + differenceInMinutes + " minutes");

                //Final Check

                if (differenceInMinutes > threshold) return true;
                return false;
            }
            else
            {
                return false;
            }
            
        }

    }

    
}
;